#!/bin/sh -eu
# derive.sh -- handy Devuan images
# Copyright (C) 2017, 2020  Olaf Meeuwissen
#
# License: GPL-3.0+

SUITE=$1
BASE_IMAGE=$CI_REGISTRY_IMAGE/slim:$SUITE

IMAGE=$CI_REGISTRY_IMAGE/builder
STAMP=$(date +%F)

docker build \
       --build-arg DEVUAN_IMAGE=$BASE_IMAGE \
       --tag $IMAGE:$SUITE-$STAMP \
       --file $(basename $IMAGE).df .

docker push $IMAGE:$SUITE-$STAMP
docker tag  $IMAGE:$SUITE-$STAMP $IMAGE:$SUITE
docker push $IMAGE:$SUITE
if test $SUITE = $DEVUAN_RELEASE; then
    docker tag  $IMAGE:$SUITE $IMAGE
    docker push $IMAGE
fi
